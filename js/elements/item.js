import { createDiv, createImg, createInput } from "../helpers/ui.js";

export class Item {
  _container = createDiv("item");
  _name = createDiv("name");
  _value = createDiv("value");
  _quantity = createInput("quantity", "number");
  _summary = createDiv("summary");
  _deleteIcon = createImg("assets/delete.svg");

  constructor(id) {
    this._id = id;
  }

  bindEvents(controller) {
    this._quantity.addEventListener("change", (event) => {
      controller.handleQuantityChange(event, this._id);
    });
    this._deleteIcon.addEventListener("click", () => {
      controller.handleItemDelete(this._id);
    });
  }

  build(name, value, quantity, summary) {
    this.setName(name);
    this.setValue(value);
    this.setQuantity(quantity);
    this.setSummary(summary);

    this._container.appendChild(this._name);
    this._container.appendChild(this._value);
    this._container.appendChild(this._quantity);
    this._container.appendChild(this._summary);
    this._container.appendChild(this._deleteIcon);

    return this._container;
  }

  getContainer() {
      return this._container;
  }

  getId() {
      return this._id;
  }

  getSummary() {
    return parseFloat(this._summary.innerText);
}

  setName(name) {
    this._name.innerText = name;
  }

  setValue(value) {
    this._value.innerText = value;
  }

  setQuantity(quantity) {
    this._quantity.value = quantity;
  }

  setSummary(summary) {
    this._summary.innerText = summary;
  }
}
