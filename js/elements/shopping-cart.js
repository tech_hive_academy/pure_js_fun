import { createDiv, emptyContent, createP } from "../helpers/ui.js";
import { Item } from "../elements/item.js";

export class ShoppingCart {
  _container = createDiv();
  _summary = createDiv("summary");
  _summary_value = createP("Suma: ");
  _items = [];

  bindEvents(controller) {
    this._items.forEach((item) => item.bindEvents(controller));
  }

  updateItem(changedItem) {
    this._items.forEach((item) => {
      if (item.getId() === changedItem.id) {
        item.setSummary(changedItem.summary);
      }
    });
    this.updateSum(this._items);
  }

  removeItem(id) {
    const found = this._items.find((item) => item.getId() === id);
    this._container.removeChild(found?.getContainer());
    this._items.splice(
      this._items.findIndex((item) => item.getId() === id),
      1
    );
  }

  addItem(id, name, value, quantity, summary) {
    const item = new Item(id);
    this._items.push(item);
    this._container.appendChild(item.build(name, value, quantity, summary));
  }

  updateSum() {
    let sum = 0;
    this._items.forEach((item) => (sum += item.getSummary()));
    this._summary_value.innerText = `Suma: ${sum}`
  }

  build(data) {
    this._items = [];
    emptyContent(this._container);
    for (let i = 0; i < data.length; i++) {
      const { id, name, value, quantity, summary } = data[i];
      this.addItem(id, name, value, quantity, summary);
    }
    this._summary.appendChild(this._summary_value);
    this._container.appendChild(this._summary);

    return this._container;
  }
}
