import { createDiv, emptyContent } from "../helpers/ui.js";

export class Content {
  _content = createDiv("content");
  _content_snapshot = {};

  build() {
    return this._content;
  }

  setSnapshot(element) {
    this._content_snapshot = element;
  }

  getSnapshot() {
    return this._content_snapshot;
  }

  addContent(element) {
    this._content.appendChild(element);
  }

  removeContent(element) {
    this._content.removeChild(element);
  }

  emptyContent() {
    emptyContent(this._content);
  }

}
