import { createDiv, createImg, createP, createAcronym } from '../helpers/ui.js';

export class Toolbar {
    _toolbar = createDiv('toolbar');
    _logo = createDiv('logo');
    _logo_img = createImg('assets/violin.svg');
    _logo_title = createP('MUSIC STORE');
    _user = createDiv('user');
    _user_acronym = createAcronym('Jan Kowalski');
    _user_name = createP('Jan Kowalski');

    build() {
        this._logo.appendChild(this._logo_img);
        this._logo.appendChild(this._logo_title);
        this._toolbar.appendChild(this._logo);
        this._user.appendChild(this._user_acronym);
        this._user.appendChild(this._user_name);
        this._toolbar.appendChild(this._user);

        return this._toolbar;
    }
}