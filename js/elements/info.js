import { createDiv } from "../helpers/ui.js";

export class Info {
    _container = createDiv('info');

    build(text) {
        this._container.innerText = text;
        return this._container;
    }

    getContainer() {
        return this._container;
    }

    setText(text) {
        this._container.classList.remove('error');
        this._container.innerText = text;
    }

    setError(text) {
        this._container.classList.add('error');
        this._container.innerText = text || 'An error occured';
    }

}