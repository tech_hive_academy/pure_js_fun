export class Controller {
  constructor(view, model) {
    this._view = view;
    this._model = model;
  }

  handleQuantityChange(event, id) {
    const quantity = event.target.value;
    const item = this._model.getItem(id);
    const newItem = { ...item, quantity };
    this._model.updateItem(id, newItem);
    this._view.updateView(this._model.getItem(id), 'update');
  }

  handleItemDelete(id) {
    this._model.removeItem(id);
    this._view.updateView(id, 'delete');
  }

  getItems(url, onInit, onSuccess, onError) {
    onInit("Loading...");
    return fetch(url, { method: "GET" })
      .then((response) => response.json())
      .then(onSuccess)
      .catch(onError);
  }
}
