export class Model {
  _items = [];

  newItems(items) {
    this._items = items.map((x) => {
      return { ...x, summary: x.value * x.quantity };
    });
  }

  getItems() {
    return this._items;
  }

  getItem(id) {
    return this._items.find((x) => x.id === id);
  }

  addItem(item) {
    this._items.push(item);
  }

  removeItem(id) {
    this._items.splice(
      this._items.findIndex((x) => x.id === id),
      1
    );
  }

  updateItem(id, newItem) {
    this.newItems(this._items.map((item) => (item.id === id ? newItem : item)));
  }
}
