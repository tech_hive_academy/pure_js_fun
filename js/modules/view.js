import { Info } from "../elements/info.js";
import { Content } from "../elements/content.js";
import { Toolbar } from "../elements/toolbar.js";
import { ShoppingCart } from "../elements/shopping-cart.js";

export class View {
  _toolbar = new Toolbar();
  _content = new Content();
  _info = new Info();

  build(parent) {
    parent.appendChild(this._toolbar.build());
    this._content.addContent(this._info.build());
    parent.appendChild(this._content.build());
  }

  setInitView(text) {
    this._info.setText(text);
  }

  setDataView(data) {
    const shoppingCart = new ShoppingCart();
    const cart = shoppingCart.build(data);
    this._content.emptyContent();
    this._content.addContent(cart);
    this._content.setSnapshot(shoppingCart);
    this._content.getSnapshot()?.updateSum();
  }

  setErrorView(text) {
    this._info.setError(text);
  }

  updateView(changedData, action) {
    // TODO find diff and take proper action - update or remove
    if (action === 'create') {}
    if (action === 'update') {
        this._content.getSnapshot()?.updateItem(changedData);
        this._content.getSnapshot()?.updateSum();
    }
    if (action === 'delete') {
        this._content.getSnapshot()?.removeItem(changedData);
        this._content.getSnapshot()?.updateSum();
    }
  }

  bindEvents(controller) {
    this._content.getSnapshot()?.bindEvents(controller);
  }
}
