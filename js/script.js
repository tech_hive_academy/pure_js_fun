import { Controller } from "./modules/controller.js";
import { Model } from "./modules/model.js";
import { View } from "./modules/view.js";

(function (id) {
  const parent = document.getElementById(id);

  const model = new Model();
  const view = new View();
  const controller = new Controller(view, model);

  view.build(parent);
  controller.getItems(
    "data/data.json",
    view.setInitView.bind(view),
    (items) => {
      model.newItems(items);
      view.setDataView(model.getItems());
      view.bindEvents(controller);
    },
    view.setErrorView.bind(view)
  );
})("shopping_cart");
