export const createDiv = (className) => {
  const div = document.createElement("div");
  div.classList.add(className);
  return div;
};

export const createInput = (className, type, onChange) => {
  const input = document.createElement("input");
  input.classList.add(className);
  input.type = type;
  input.onchange = onChange;
  return input;
};

export const createImg = (src) => {
  const img = document.createElement("img");
  img.src = src;
  return img;
};

export const createP = (text) => {
  const p = document.createElement("p");
  p.innerText = text;
  return p;
};

export const createAcronym = (name) => {
  const acronym = document.createElement("div");
  acronym.classList.add("acronym");
  const text = `${name.split(" ").map((word) => word.charAt(0).toUpperCase()).join("")}`;
  acronym.innerText = text;
  return acronym;
};

export const emptyContent = (element) => {
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
}
